﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Devsu2019
{
    class Program
    {
        static void Main(string[] args)
        {
            Ejercicio1();
            Ejercicio2();
            Ejercicio3(3, "Greetings, this is AN EXAMPLE!");
            Ejercicio4(3);
            Ejercicio5("ll", "hllalls");
            Ejercicio15(3421);

        }

        // Ejercicio 1
        static void Ejercicio1 ()
        {
            string key = "dcj";
            string message = "I love prOgrAmming!";

            var aux = new List<string>();

            foreach (char ch in message)
            {
                if (ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' ||ch == 'U' || ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u')
                {
                    string aux2 = ch.ToString();
                    aux.Add(key);
                    aux.Add(aux2);
                }
                else
                {
                    string aux3 = ch.ToString();
                    aux.Add(aux3);
                }
            }
            string something = string.Join("", aux.ToArray());

            Console.WriteLine("Ej1: "+something);
        }

        // Ejercicio 2
        static void Ejercicio2()
        {
            long[] times = { 65647440, 199644521 };

            long[] respuesta = new long[5];

            TimeSpan t = TimeSpan.FromMilliseconds(0);

            foreach (long time in times)
            {
                if (time >= 0)
                {
                    t = t + TimeSpan.FromMilliseconds(time);
                }
                
            }

            respuesta[0] = t.Days;
            respuesta[1] = t.Hours;
            respuesta[2] = t.Minutes;
            respuesta[3] = t.Seconds;
            respuesta[4] = t.Milliseconds;

            foreach (var item in respuesta)
            {
                Console.WriteLine("Ej2: " + item.ToString());
            }

        }

        // Ejercicio 3
        static string Ejercicio3(int n, string message)
        {
            int cont = 0;

            if (message == "")
            {
                return "";
            }

            if (n <= 0)
            {
                return message;
            }

            foreach (char letter in message)
            {
                cont++;
                if (cont == n)
                {
                    if (char.IsLower(letter))
                    {
                        char.ToUpper(letter);
                    } else
                    {
                        char.ToLower(letter);
                    }
                    cont = 0;
                }
            }
            return message;
        }

        // Ejercicio 4
        static int Ejercicio4(int n)
        {
            int[] series = new int[] { -3, -2, 1, 6, 13, 22, 33, 46, 61, 78 };

            if (n < 0)
            {
                Console.WriteLine("Valor ingresado incorrecto");
                return 0;
            }
            n--;
            Console.WriteLine("Ej4: "+ series[n]);
            return series[n];
            
        }

        // Ejercicio 5
        static string Ejercicio5 (string key, string message)
        {
            
            if ( string.IsNullOrEmpty(message) )
            {
                return "";
            }

            if ( string.IsNullOrEmpty(key) )
            {
                key = "DCJ";
            }

            char[] mes = message.ToCharArray();
            var list = mes.ToList();

            for (int j = 0; j < mes.Length; j++)
            {
                if (mes[j] == 'A' || mes[j] == 'E' || mes[j] == 'I' || mes[j] == 'O' || mes[j] == 'U' || mes[j] == 'a' || mes[j] == 'e' || mes[j] == 'i' || mes[j] == 'o' || mes[j] == 'u')
                {
                    for (int i = 1; i <= key.Length  ; i++)
                    {
                        int borrar = j - i;
                        list.Remove(mes[borrar]);
                    }
                }
            }
            var result = String.Join("", list.ToArray());
            Console.WriteLine("Ej5: " + result);
            return result;
        }

        // Ejercicio 15
        static int Ejercicio15(int n)
        {
            int diff = n;
            int num = 0;

            while (diff != 6174) {

                var digit = diff;
                string asc1 = String.Join("", digit.ToString().ToCharArray().OrderBy(x => x));
                int asc = Int32.Parse(asc1);

                var digit2 = diff;
                string desc1 = String.Join("", digit2.ToString().ToCharArray().OrderByDescending(x => x));
                int desc = Int32.Parse(desc1);

                diff = (desc - asc);

                num++;
            }
            Console.WriteLine("Ej6: " + num);
            return num;
        }
    }
}
